FROM ruby:2.3
MAINTAINER OEMS <oscaremu@gmail.com>

ADD Gemfile /srv/jekyll/Gemfile
ADD Gemfile.lock /srv/jekyll/Gemfile.lock

RUN cd /srv/jekyll/ && bundle install
